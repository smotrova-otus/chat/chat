package ru.lilitweb.chat.rest.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MessageRequestTest {
    private Validator validator;

    @BeforeEach
    void init() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void testValidation_Success() {
        MessageRequest r = new MessageRequest();
        r.setBody("test message");
        Set<ConstraintViolation<MessageRequest>> violations = validator.validate(r);
        assertTrue(violations.isEmpty());
    }

    @Test
    void testValidation_EmptyBody() {
        MessageRequest r = new MessageRequest();
        r.setBody("");
        Set<ConstraintViolation<MessageRequest>> violations = validator.validate(r);
        assertFalse(violations.isEmpty());
    }
}
