package ru.lilitweb.chat.rest.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.domain.Member;
import ru.lilitweb.chat.repository.ChannelRepository;
import ru.lilitweb.chat.repository.MemberRepository;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MemberController.class)
class MemberControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MemberRepository members;

    @MockBean
    private ChannelRepository channels;

    @Test
    void create_Success() throws Exception {
        UUID channelId = UUID.randomUUID();
        UUID userId1 = UUID.randomUUID();
        Channel channel = Channel.builder()
                .id(channelId)
                .ownerId(UUID.randomUUID())
                .build();
        when(channels.findById(channelId)).thenReturn(Optional.of(channel));
        when(members.findByUserIdAndChannelId(userId1, channelId)).thenReturn(Optional.empty());

        Member savedMember = Member.builder()
                .id(UUID.randomUUID())
                .userId(userId1)
                .channel(channel)
                .build();
        when(members.save(Member.builder()
                .channel(channel)
                .userId(userId1)
                .build())
        ).thenReturn(savedMember);
        mvc.perform(
                post(String.format(
                        "/v1/channels/%s/members",
                        channelId))
                        .content(String.format("{\"userId\":\"%s\"}", userId1.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("id", is(savedMember.getId().toString())))
                .andExpect(jsonPath("userId", is(savedMember.getUserId().toString())))
                .andExpect(jsonPath("channelId", is((channelId.toString()))))
                .andExpect(status().isCreated())
                .andReturn();

        verify(members, times(1)).save(Member.builder()
                .channel(channel)
                .userId(userId1)
                .build());
    }

    @Test
    void create_InvalidUuid() throws Exception {
        mvc.perform(
                post(String.format(
                        "/v1/channels/%s/members",
                        "invalid_uuid"))
                        .content(String.format("{\"userId\":\"%s\"}", UUID.randomUUID().toString()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("code", is("VALIDATION_ERROR")))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void create_ChannelNotFound() throws Exception {
        UUID channelId = UUID.randomUUID();
        when(channels.findById(channelId)).thenReturn(Optional.empty());
        mvc.perform(
                post(String.format(
                        "/v1/channels/%s/members",
                        channelId))
                        .content(String.format("{\"userId\":\"%s\"}", UUID.randomUUID().toString()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("code", is("NOT_FOUND")))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void delete_Success() throws Exception {
        UUID channelId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        UUID memberId = UUID.randomUUID();
        Channel channel = Channel.builder()
                .id(channelId)
                .ownerId(userId)
                .build();
        Member member = Member.builder()
                .id(memberId)
                .channel(channel)
                .userId(userId)
                .build();
        when(channels.findById(channelId)).thenReturn(Optional.of(channel));
        when(members.findById(memberId)).thenReturn(Optional.of(member));
        mvc.perform(
                delete(String.format(
                        "/v1/channels/%s/members/%s",
                        channelId,
                        memberId
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(content().string(""))
                .andExpect(status().isOk())
                .andReturn();
        verify(members, times(1)).delete(member);
    }

    //TODO add failed test for delete
}
