package ru.lilitweb.chat.rest.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.domain.Member;
import ru.lilitweb.chat.domain.Message;
import ru.lilitweb.chat.repository.ChannelRepository;
import ru.lilitweb.chat.repository.MemberRepository;
import ru.lilitweb.chat.repository.MessageRepository;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MessageController.class)
class MessageControllerTest {

    @MockBean
    MessageRepository messages;

    @MockBean
    ChannelRepository channels;

    @MockBean
    MemberRepository members;

    @Autowired
    MockMvc mvc;

    @Test
    void create_Success() throws Exception {
        UUID channelId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        Channel channel = Channel.builder()
                .id(channelId)
                .build();
        when(channels.findById(channelId)).thenReturn(Optional.of(channel));
        when(members.findByUserIdAndChannelId(userId, channelId)).thenReturn(Optional.of(
                Member.builder()
                        .id(UUID.randomUUID())
                        .userId(userId)
                        .channel(channel)
                        .build()
        ));
        UUID messageId = UUID.randomUUID();
        Message savedMessage = Message.builder()
                .id(messageId)
                .wasEdited(false)
                .body("some message")
                .fromId(userId)
                .to(channel)
                .build();
        when(messages.save(any())).thenReturn(savedMessage);
        mvc.perform(
                post(String.format("/v1/channels/%s/messages", channelId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.format("{\"body\": \"some message\", \"from\":\"%s\"}", userId.toString()))
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id", is(messageId.toString())))
                .andExpect(jsonPath("from", is(userId.toString())))
                .andExpect(jsonPath("to", is(channelId.toString())))
                .andExpect(jsonPath("body", is("some message")))
                .andExpect(jsonPath("wasEdited", is(false)))
                .andReturn();
        verify(messages, times(1)).save(Message.builder()
                .body("some message")
                .wasEdited(false)
                .fromId(userId)
                .to(channel)
                .build());

    }

    @Test
    void create_NotAllowed() throws Exception {
        UUID channelId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        Channel channel = Channel.builder()
                .id(channelId)
                .build();
        when(channels.findById(channelId)).thenReturn(Optional.of(channel));
        when(members.findByUserIdAndChannelId(userId, channelId)).thenReturn(Optional.empty());
        mvc.perform(
                post(String.format("/v1/channels/%s/messages", channelId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.format("{\"body\": \"some message\", \"from\":\"%s\"}", userId.toString()))
        )
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("code", is("PERMISSION_DENIED")))
                .andReturn();

    }

    @Test
    void create_WithoutRequestBody() throws Exception {
        UUID channelId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        Channel channel = Channel.builder()
                .id(channelId)
                .build();
        when(channels.findById(channelId)).thenReturn(Optional.of(channel));
        when(members.findByUserIdAndChannelId(userId, channelId)).thenReturn(Optional.empty());
        mvc.perform(
                post(String.format("/v1/channels/%s/messages", channelId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("MALFORMED_JSON_REQUEST")))
                .andReturn();

    }

    @Test
    void create_ChannelNotFound() throws Exception {
        UUID channelId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        when(channels.findById(channelId)).thenReturn(Optional.empty());
        mvc.perform(
                post(String.format("/v1/channels/%s/messages", channelId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.format("{\"body\": \"some message\", \"from\":\"%s\"}", userId.toString()))
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("code", is("NOT_FOUND")))
                .andReturn();
    }
}
