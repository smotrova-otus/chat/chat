package ru.lilitweb.chat.rest.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.domain.Member;
import ru.lilitweb.chat.repository.ChannelRepository;
import ru.lilitweb.chat.repository.MemberRepository;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ChannelController.class)
class ChannelControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    ChannelRepository channels;

    @MockBean
    MemberRepository members;

    @Test
    void create_Success() throws Exception {
        UUID userId = UUID.randomUUID();
        Channel channel = Channel.builder()
                .code("codeChan1")
                .name("chan1")
                .ownerId(userId)
                .build();
        when(channels.findByCode("codeChan1")).thenReturn(Optional.empty());

        Channel savedChannel = Channel.builder()
                .id(UUID.randomUUID())
                .code("codeChan1")
                .name("chan1")
                .ownerId(userId)
                .build();
        when(channels.save(any())).thenReturn(savedChannel);
        when(members.save(any())).thenReturn(Member.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .channel(savedChannel)
                .build());
        mvc.perform(
                post("/v1/channels")
                        .content(String.format("{\"name\": \"chan1\",\"code\": \"codeChan1\",\"ownerId\":\"%s\"}",
                                userId.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("id", is(savedChannel.getId().toString())))
                .andExpect(jsonPath("name", is(savedChannel.getName())))
                .andExpect(jsonPath("code", is(savedChannel.getCode())))
                .andExpect(jsonPath("ownerId", is(savedChannel.getOwnerId().toString())))
                .andExpect(status().isCreated())
                .andReturn();

        verify(channels, times(1)).save(channel);
        verify(members, times(1)).save(Member.builder()
                .channel(savedChannel)
                .userId(userId)
                .build());
    }

    @Test
    void create_ChannelAlreadyExist() throws Exception {
        UUID userId = UUID.randomUUID();
        Channel channel = Channel.builder()
                .id(UUID.randomUUID())
                .code("codeChan1")
                .name("chan1")
                .ownerId(userId)
                .build();
        when(channels.findByCode("codeChan1")).thenReturn(Optional.of(channel));
        mvc.perform(
                post("/v1/channels")
                        .content(String.format("{\"name\": \"chan1\",\"code\": \"codeChan1\",\"ownerId\":\"%s\"}",
                                userId.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(jsonPath("code", is("CHANNEL_ALREADY_EXIST")))
                .andExpect(status().isBadRequest())
                .andReturn();

        verify(channels, times(0)).save(any());
    }

    // TODO add test invalid or empty ownerId
}
