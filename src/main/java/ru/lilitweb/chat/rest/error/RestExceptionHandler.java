package ru.lilitweb.chat.rest.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.lilitweb.chat.rest.ChannelAlreadyExistException;
import ru.lilitweb.chat.rest.NotFoundException;
import ru.lilitweb.chat.rest.error.converter.ConstraintViolationToApiValidationError;
import ru.lilitweb.chat.rest.error.converter.FieldErrorToApiValidationConverter;
import ru.lilitweb.chat.rest.error.converter.ObjectErrorToApiValidationError;

import java.util.Objects;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    private final String VALIDATION_ERROR = "VALIDATION_ERROR";

    /**
     * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter is missing.
     *
     * @param ex      MissingServletRequestParameterException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        return toResponseEntity(new ApiError(
                HttpStatus.BAD_REQUEST,
                error,
                "INPUT_PARAMETER_MISSING",
                ex));
    }

    /**
     * Customize the response for ServletRequestBindingException.
     * <p>This method delegates to {@link #handleExceptionInternal}.
     * @param ex the exception
     * @param headers the headers to be written to the response
     * @param status the selected response status
     * @param request the current request
     * @return a {@code ResponseEntity} instance
     */
    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(
            ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        return toResponseEntity(new ApiError(
                HttpStatus.BAD_REQUEST,
                ex.getMessage(),
                "HEADER_MISSING",
                ex));
    }

    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.BAD_REQUEST)
                .message("validation error")
                .code(VALIDATION_ERROR)
                .build();
        ex.getBindingResult().getFieldErrors()
                .forEach(fError -> apiError.addError((new FieldErrorToApiValidationConverter()).convert(fError)));
        ex.getBindingResult().getGlobalErrors()
                .forEach(obError -> apiError.addError((new ObjectErrorToApiValidationError()).convert(obError)));
        return toResponseEntity(apiError);
    }

    /**
     * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
     *
     * @param ex      HttpMessageNotReadableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return toResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, "malformed JSON request",
                "MALFORMED_JSON_REQUEST", ex));
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param ex the ConstraintViolationException
     * @return ApiError object
     */
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
            javax.validation.ConstraintViolationException ex) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.BAD_REQUEST)
                .message("validation error")
                .code(VALIDATION_ERROR)
                .build();
        ex.getConstraintViolations().
                forEach(cError -> apiError.addError((new ConstraintViolationToApiValidationError()).convert(cError)));
        return toResponseEntity(apiError);
    }

    /**
     * Handle Exception, handle generic MethodArgumentTypeMismatchException.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.BAD_REQUEST)
                .message(String.format("The parameter '%s' of value '%s' could not be converted to type '%s'",
                        ex.getName(),
                        ex.getValue(),
                        Objects.requireNonNull(ex.getRequiredType()).getSimpleName()))
                .debugMessage(ex.getMessage())
                .code(VALIDATION_ERROR)
                .build();
        return toResponseEntity(apiError);
    }

    /**
     * Handle Exception, handle generic NotFoundException.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleMethodNotFound(NotFoundException ex,
                                                          WebRequest request) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.NOT_FOUND)
                .message("not found")
                .debugMessage(ex.getMessage())
                .code("NOT_FOUND")
                .build();
        return toResponseEntity(apiError);
    }

    /**
     * Handle Exception, handle generic ChannelAlreadyExistException.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(ChannelAlreadyExistException.class)
    protected ResponseEntity<Object> handleMethodChannelAlreadyExist(ChannelAlreadyExistException ex,
                                                          WebRequest request) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.BAD_REQUEST)
                .message("channel already exist")
                .debugMessage(ex.getMessage())
                .code("CHANNEL_ALREADY_EXIST")
                .build();
        return toResponseEntity(apiError);
    }

    /**
     * Handle Exception, handle generic PermissionDeniedDataAccessException.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(PermissionDeniedDataAccessException.class)
    protected ResponseEntity<Object> handleMethodPermissionDenied(PermissionDeniedDataAccessException ex,
                                                          WebRequest request) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.FORBIDDEN)
                .message("operation not allowed")
                .debugMessage(ex.getMessage())
                .code("PERMISSION_DENIED")
                .build();
        return toResponseEntity(apiError);
    }

    //MissingRequestHeaderException

    /**
     * Handle another exception
     *
     * @param ex      Exception
     * @param request WebRequest
     * @return ApiError object
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        ApiError apiError = ApiError.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .code("INTERNAL_ERROR")
                .message("error occurred")
                .debugMessage(ex.getLocalizedMessage())
                .build();
        logger.error(apiError.getMessage(), ex);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    private ResponseEntity<Object> toResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
