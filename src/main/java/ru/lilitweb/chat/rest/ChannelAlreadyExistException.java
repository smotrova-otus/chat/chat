package ru.lilitweb.chat.rest;


public class ChannelAlreadyExistException extends RuntimeException {
    /**
     * Constructs a new runtime exception with username
     * @param username string
     */
    public ChannelAlreadyExistException(String username) {
        super(String.format("Channel %s already exist", username));
    }
}
