package ru.lilitweb.chat.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.domain.Member;
import ru.lilitweb.chat.repository.ChannelRepository;
import ru.lilitweb.chat.repository.MemberRepository;
import ru.lilitweb.chat.rest.ChannelAlreadyExistException;
import ru.lilitweb.chat.rest.converter.ChannelToDTOConverter;
import ru.lilitweb.chat.rest.converter.MemberToDTOConverter;
import ru.lilitweb.chat.rest.dto.ChannelDTO;
import ru.lilitweb.chat.rest.dto.MemberDTO;
import ru.lilitweb.chat.rest.error.ApiError;


import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("v1")
public class ChannelController {

    private ChannelRepository channels;
    private MemberRepository members;

    @Autowired
    public ChannelController(ChannelRepository channels, MemberRepository members) {
        this.channels = channels;
        this.members = members;
    }

    @ApiOperation("Create channel and add current user to channel")
    @PostMapping("/channels")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad request, code = VALIDATION_ERROR, CHANNEL_ALREADY_EXIST, MALFORMED_JSON_REQUEST",
                    response = ApiError.class),
            @ApiResponse(code = 500, message = "Internal Server Error, code = INTERNAL_ERROR",
                    response = ApiError.class),
    })
    @Transactional
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<ChannelDTO> create(
            @Valid @RequestBody @NotBlank ChannelRequest data,
            ChannelToDTOConverter converter,
            MemberToDTOConverter memberConverter
            ) {
        String code = data.getCode();
        if (channels.findByCode(code).isPresent()) {
            throw new ChannelAlreadyExistException(code);
        }
        Channel ch = channels.save(Channel.builder()
                .name(data.getName())
                .code(data.getCode())
                .ownerId(data.getOwnerId())
                .build());
        MemberDTO m = memberConverter.convert(members.save(Member.builder()
                .userId(data.getOwnerId())
                .channel(ch)
                .build()));
        if (m == null) {
            throw new NullPointerException("empty member dto");
        }
        URI uri = MvcUriComponentsBuilder.fromController(getClass()).path("/channel/{id}")
                .buildAndExpand(ch.getId()).toUri();
        return ResponseEntity.created(uri).body(converter.convert(ch));
    }
}
