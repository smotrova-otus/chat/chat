package ru.lilitweb.chat.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.domain.Message;
import ru.lilitweb.chat.repository.ChannelRepository;
import ru.lilitweb.chat.repository.MemberRepository;
import ru.lilitweb.chat.repository.MessageRepository;
import ru.lilitweb.chat.rest.NotFoundException;
import ru.lilitweb.chat.rest.converter.MessageToDTOConverter;
import ru.lilitweb.chat.rest.dto.MessageDTO;
import ru.lilitweb.chat.rest.error.ApiError;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("v1")
public class MessageController {

    private ChannelRepository channels;

    private MemberRepository members;

    private MessageRepository messages;

    @Autowired
    public MessageController(
            MessageRepository messages,
            MemberRepository members,
            ChannelRepository channels
    ) {
        this.messages = messages;
        this.members = members;
        this.channels = channels;
    }

    @ApiOperation("Add message to channel")
    @PostMapping("/channels/{channelId}/messages")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad request, code = VALIDATION_ERROR, MALFORMED_JSON_REQUEST",
                    response = ApiError.class),
            @ApiResponse(code = 404, message = "Bad request, code = NOT_FOUND",
                    response = ApiError.class),
            @ApiResponse(code = 403, message = "Not allowed write in this channel, code = PERMISSION_DENIED",
                    response = ApiError.class),
            @ApiResponse(code = 500, message = "Internal Server Error, code = INTERNAL_ERROR",
                    response = ApiError.class),
    })
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<MessageDTO> create(
            @Valid @RequestBody MessageRequest data,
            @PathVariable UUID channelId,
            MessageToDTOConverter converter) {
        Channel channel = channels.findById(channelId)
                .orElseThrow(() -> new NotFoundException("channel not found"));
        members.findByUserIdAndChannelId(data.getFrom(), channel.getId())
                .orElseThrow(() -> new PermissionDeniedDataAccessException("not allowed write in this channel", null));

        Message m = messages.save(Message.builder()
                .fromId(data.getFrom())
                .to(channel)
                .body(data.getBody())
                .wasEdited(false)
                .build());
        URI uri = MvcUriComponentsBuilder.fromController(getClass()).path("/channels/{channelId}/messages/{id}")
                .buildAndExpand(channelId, m.getId()).toUri();
        MessageDTO dto = converter.convert(m);
        if (dto == null) { // It is impossible case
            throw new NullPointerException("empty message dto");
        }
        return ResponseEntity.created(uri).body(dto);
    }
}
