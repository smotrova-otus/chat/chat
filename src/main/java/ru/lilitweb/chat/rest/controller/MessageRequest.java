package ru.lilitweb.chat.rest.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class MessageRequest implements Serializable {
    @ApiModelProperty(value = "body", required = true, example = "some message")
    @Size(min = 1)
    private String body;

    @ApiModelProperty(value = "from",
            required = true,
            notes = "it is userId of message author",
            example = "48171497-619d-448b-a34a-8a7da1253908")
    // TODO add validation
    private UUID from;
}
