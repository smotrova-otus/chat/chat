package ru.lilitweb.chat.rest.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class ChannelRequest implements Serializable {
    @ApiModelProperty(value = "name", required = true, example = "Онлайн курсы от Otus")
    @Size(min = 1, max = 1024)
	private String name;

    @ApiModelProperty(value = "code", required = true, example = "otus")
    @Size(min = 1, max = 1024)
    private String code;

    @ApiModelProperty(value = "ownerId", required = true, example = "48171497-619d-448b-a34a-8a7da1253908")
    // TODO add validation
    private UUID ownerId;
}
