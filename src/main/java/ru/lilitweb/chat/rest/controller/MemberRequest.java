package ru.lilitweb.chat.rest.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class MemberRequest implements Serializable {
    @ApiModelProperty(value = "userId", required = true, example = "48171497-619d-448b-a34a-8a7da1253908")
    // TODO add validation
    private UUID userId;
}
