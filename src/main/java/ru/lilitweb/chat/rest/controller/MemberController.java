package ru.lilitweb.chat.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.domain.Member;
import ru.lilitweb.chat.repository.ChannelRepository;
import ru.lilitweb.chat.repository.MemberRepository;
import ru.lilitweb.chat.rest.NotFoundException;
import ru.lilitweb.chat.rest.converter.MemberToDTOConverter;
import ru.lilitweb.chat.rest.dto.MemberDTO;
import ru.lilitweb.chat.rest.error.ApiError;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("v1")
public class MemberController {

    private final MemberRepository members;
    private final ChannelRepository channels;

    @Autowired
    public MemberController(
            MemberRepository members,
            ChannelRepository channels) {

        this.members = members;
        this.channels = channels;
    }

    @ApiOperation("Add members to channel")
    @PostMapping("/channels/{channelId}/members")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, code = VALIDATION_ERROR, MALFORMED_JSON_REQUEST",
                    response = ApiError.class),
            @ApiResponse(code = 404, message = "Not found channel, code = NOT_FOUND",
                    response = ApiError.class),
            @ApiResponse(code = 500, message = "Internal Server Error, code = INTERNAL_ERROR",
                    response = ApiError.class),
    })
    public ResponseEntity<MemberDTO> create(
            @PathVariable UUID channelId,
            @Valid @RequestBody @NotBlank MemberRequest data,
            MemberToDTOConverter memberConverter
    ) {
        Channel channel = channels.findById(channelId).orElseThrow(() -> new NotFoundException("channel not found"));
        Optional<Member> optionalMember = members.findByUserIdAndChannelId(data.getUserId(), channel.getId());
        MemberDTO m;
        if (optionalMember.isEmpty()) {
            m = memberConverter.convert(members.save(Member.builder()
                    .userId(data.getUserId())
                    .channel(channel)
                    .build()));

        } else {
            m = memberConverter.convert(optionalMember.get());
            if (m == null) {
                throw new NullPointerException("empty member dto");
            }
        }

        URI uri = MvcUriComponentsBuilder.fromController(getClass()).path("/channel/{channelId}/members/{id}")
                .buildAndExpand(channelId, m.getId()).toUri();
        return ResponseEntity.created(uri).body(m);
    }

    @ApiOperation("Remove member from channel")
    @DeleteMapping("/channels/{channelId}/members/{memberId}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, code = VALIDATION_ERROR",
                    response = ApiError.class),
            @ApiResponse(code = 404, message = "Not found channel or member, code = NOT_FOUND",
                    response = ApiError.class),
            @ApiResponse(code = 403, message = "Not allowed in this channel, code = PERMISSION_DENIED",
                    response = ApiError.class),
            @ApiResponse(code = 500, message = "Internal Server Error, code = INTERNAL_ERROR",
                    response = ApiError.class),
    })

    public void delete(
            @PathVariable UUID channelId,
            @PathVariable UUID memberId
    ) {

        Member member = members.findById(memberId)
                .orElseThrow(() -> new NotFoundException("member not found"));
        if (member.getChannel() == null) {
            throw new NotFoundException("channel not found");
        }
        if (!channelId.equals(member.getChannel().getId())) {
            // TODO move it in auth service
            throw new PermissionDeniedDataAccessException("not allowed in this channel", null);
        }

        members.delete(member);
    }
}
