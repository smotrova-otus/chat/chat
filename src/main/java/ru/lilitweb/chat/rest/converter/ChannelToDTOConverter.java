package ru.lilitweb.chat.rest.converter;

import org.springframework.core.convert.converter.Converter;
import ru.lilitweb.chat.domain.Channel;
import ru.lilitweb.chat.rest.dto.ChannelDTO;

public class ChannelToDTOConverter implements Converter<Channel, ChannelDTO> {
    @Override
    public ChannelDTO convert(Channel source) {
        return ChannelDTO.builder()
                .code(source.getCode())
                .name(source.getName())
                .ownerId(source.getOwnerId())
                .id(source.getId())
                .build();
    }
}
