package ru.lilitweb.chat.rest.converter;

import org.springframework.core.convert.converter.Converter;
import ru.lilitweb.chat.domain.Member;
import ru.lilitweb.chat.rest.dto.MemberDTO;

public class MemberToDTOConverter implements Converter<Member, MemberDTO> {
    @Override
    public MemberDTO convert(Member source) {
        return MemberDTO.builder()
                .id(source.getId())
                .userId(source.getUserId())
                .channelId(source.getChannel().getId())
                .build();
    }
}
