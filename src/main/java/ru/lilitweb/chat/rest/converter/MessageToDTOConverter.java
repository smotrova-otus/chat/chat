package ru.lilitweb.chat.rest.converter;

import org.springframework.core.convert.converter.Converter;
import ru.lilitweb.chat.domain.Message;
import ru.lilitweb.chat.rest.dto.MessageDTO;

public class MessageToDTOConverter implements Converter<Message, MessageDTO> {
    @Override
    public MessageDTO convert(Message source) {
        return MessageDTO.builder()
                .id(source.getId())
                .body(source.getBody())
                .wasEdited(source.getWasEdited())
                .from(source.getFromId())
                .to(source.getTo().getId())
                .build();
    }
}
