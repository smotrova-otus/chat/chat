package ru.lilitweb.chat.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChannelDTO {
    UUID id;

    UUID ownerId;

    String name;

    String code;
}
