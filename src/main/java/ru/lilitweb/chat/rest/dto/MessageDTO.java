package ru.lilitweb.chat.rest.dto;

import lombok.*;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MessageDTO {
    UUID id;

    Boolean wasEdited;

    UUID from;

    UUID to;

    String body;
}
