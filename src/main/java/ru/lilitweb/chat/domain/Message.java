package ru.lilitweb.chat.domain;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Entity
@Table(name = "messages")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id;

    @NonNull
    Boolean wasEdited;

    @Column(nullable = false)
    UUID fromId;

    @ManyToOne()
    @NonNull
    Channel to;

    @NotEmpty
    @Column(columnDefinition = "TEXT")
    String body;
}
