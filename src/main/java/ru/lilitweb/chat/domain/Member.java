package ru.lilitweb.chat.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "members", uniqueConstraints = { @UniqueConstraint( columnNames = { "userId", "channel_id" })})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    Channel channel;

    @Column(nullable = false)
    UUID userId;
}
