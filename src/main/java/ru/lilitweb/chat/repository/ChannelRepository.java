package ru.lilitweb.chat.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.lilitweb.chat.domain.Channel;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ChannelRepository extends CrudRepository<Channel, UUID> {
    Optional<Channel> findByCode(String code);
}
