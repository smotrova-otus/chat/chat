FROM openjdk:13-jdk-alpine as BUILD_IMAGE

# Get gradle distribution
COPY *.gradle gradle.* gradlew /src/
COPY gradle /src/gradle
WORKDIR /src
RUN ./gradlew --version

COPY . .
RUN ./gradlew build

FROM openjdk:13-jdk-alpine

VOLUME /tmp
COPY --from=BUILD_IMAGE /src/build/libs/*SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
